import json
import os
import sys
import logging

year = os.environ.get("CVE_YEAR", "2023")
dirpath = f"cves/{year}/"

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_handler.setFormatter(formatter)

log.addHandler(console_handler)


num_cves = 0
for root, dirs, files in os.walk(dirpath):
    # log.info(root, dirs)
    for file in files:
        found = False
        filepath = os.path.join(root, file)
        with open(filepath, "r") as fp:
            content = json.load(fp)
            if content["cveMetadata"]["state"] == "PUBLISHED":
                for item in content["containers"]["cna"]["affected"]:
                    for key in item.keys():
                        if key == "product":
                            product = item["product"].lower()
                            if (
                                "linux" in product
                                and "kernel" in product
                                and "microsoft" not in product
                                and "vmware" not in product
                                and "windows" not in product
                                and "endpoint" not in product
                                and "nvidia" not in product
                            ):
                                num_cves += 1
                                metrics = content["containers"]["cna"].get("metrics")
                                severity = "Not listed"
                                if metrics:
                                    for metric in metrics:
                                        if metric.get("cvssV3_1"):
                                            severity = metric.get("cvssV3_1").get("baseSeverity")
                                log.info("########################################################")
                                log.info(filepath)
                                log.info(f"Product ==> {item['product']}")
                                log.info(f"Severity ==> {severity}")
                                log.info(f"Title ==> {content['containers']['cna'].get('title')}")
                                for description in content["containers"]["cna"].get("descriptions"):
                                    if description.get("value"):
                                        log.info(f"Description:\n{description.get('value')}")
                                log.info("########################################################\n")
                            found = True
            else:
                found = True
        if not found:
            log.info("ERROR: product not found in CVE content")
            log.info(filepath)

log.info("########################################################")
log.info(f"Total Number of Linux kernel related CVEs are: [{num_cves}]")
log.info("########################################################")